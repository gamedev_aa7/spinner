﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialNetworks : MonoBehaviour {

	public static SocialNetworks instance;

	public string vk;
	public string twitter;
	public string facebook;

	void Start () {
		instance = gameObject.GetComponent<SocialNetworks> ();
	}

	public static bool FacebookState
	{
		get { return PlayerPrefs.GetInt("fb", 0) > 0 ? true : false; }
		set
		{
			if (value)
			{
				PlayerPrefs.SetInt("fb", 1);
			}
			else
			{
				PlayerPrefs.SetInt("fb", 0);
			}
		}
	}

	public static bool TwitterState
	{
		get { return PlayerPrefs.GetInt("tw", 0) > 0 ? true : false; }
		set
		{
			if (value)
			{
				PlayerPrefs.SetInt("tw", 1);
			}
			else
			{
				PlayerPrefs.SetInt("tw", 0);
			}
		}
	}
}
