﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Heyzap;

public class GameController : MonoBehaviour {

	public GameScreenController gsc;

	[Header("LevelsType settings")]
	public LevelType[] pattern;
	public LevelType[] patternCore;

	[Header("Swipe settings")]
	public Vector3 dir;

	[Header("Camera settings")]
	public CameraController cam;

	[Header("Finger settings")]
	public float nextFingerDistance;
	public Vector2 deltaX;
	public GameObject finger;
	private float actualPos = 0;
	private List<GameObject> fingers = new List<GameObject> ();
	private float prevX = 0;

	[Header("Spinner settings")]
	public SkinsController skinsController;
	private GameObject spinner;
	private GameObject instanceSpinner = null;
	public float rotateSpeed;

	void Start(){
		SwipeManager.gameState = true;
		skinsController.SetUpdateSkinsCallback( UpdateSkin);
		UpdateSkin ();
	}

	public void UpdateSkin(){
		spinner = skinsController.GetActive ().skin;
		if (instanceSpinner != null) {			
			//Vector3 pos = instanceSpinner.transform.position;
			//Destroy (instanceSpinner);
			//instanceSpinner = Instantiate (spinner);
			//instanceSpinner.transform.position = pos;
			//instanceSpinner.GetComponent<SpinnerController> ().StartRotate (rotateSpeed);
		}
	}

	private void ClearVars(){
		prevX = 0;
		actualPos = 0;
		Settings.Coins = 0;
	}

	public void StartGame(){
		//Destroy ();
		ClearScene ();
		SwipeManager.gameState = true;
		ClearVars ();
		fingers.Add (Instantiate(finger));
		UpdateSkin ();
		instanceSpinner = Instantiate (spinner);
		instanceSpinner.GetComponent<SpinnerController> ().contr = gameObject.GetComponent<GameController> ();
		instanceSpinner.GetComponent<SpinnerController> ().StartRotate (rotateSpeed);
	}

	public void NextFinger(){
		Settings.Coins++;
		GameObject f = Instantiate(finger);
		actualPos += nextFingerDistance;

		int i = 0;
		LevelType[] p = pattern;
		if (Settings.Coins < pattern.Length) {
			i = Settings.Coins;
		} else {
			p = patternCore;
			if ((Settings.Coins - pattern.Length) < patternCore.Length) {
				i = Settings.Coins - pattern.Length;
			} else {
				i = ((Settings.Coins - pattern.Length) % patternCore.Length);
			}

		}
		switch(p[i]){
		case LevelType.Easy:
			f.transform.position = new Vector3 (0,0, actualPos);
			f.GetComponent<FingerController> ().MoveToStart (Settings.Coins % 2 >= 1? true:false, FingerController.MoveDir.None, LevelType.Easy);
			break;
		case LevelType.Default:
			f.transform.position = new Vector3 (0, 0, actualPos);
			f.GetComponent<FingerController> ().posX = UnityEngine.Random.Range (deltaX.x, deltaX.y);
			f.GetComponent<FingerController> ().MoveToStart (Settings.Coins % 2 >= 1? true:false, FingerController.MoveDir.None, LevelType.Default);
			break;
		case LevelType.Hard:
			f.transform.position = new Vector3 (0, 0, actualPos);
			f.GetComponent<FingerController> ().posX = UnityEngine.Random.Range (deltaX.x, deltaX.y);
			f.GetComponent<FingerController> ().MoveToStart (Settings.Coins % 2 >= 1? true:false, FingerController.MoveDir.Left, LevelType.Hard);
			break;
		case LevelType.Hard1:
			f.transform.position = new Vector3 (0, 0, actualPos);
			f.GetComponent<FingerController> ().posX = UnityEngine.Random.Range (deltaX.x, deltaX.y);
			f.GetComponent<FingerController> ().MoveToStart (Settings.Coins % 2 >= 1? true:false, FingerController.MoveDir.Left, LevelType.Hard1);
			break;
		case LevelType.Hard2:
			f.transform.position = new Vector3 (0, 0, actualPos);
			f.GetComponent<FingerController> ().posX = UnityEngine.Random.Range (deltaX.x, deltaX.y);
			f.GetComponent<FingerController> ().MoveToStart (Settings.Coins % 2 >= 1? true:false, FingerController.MoveDir.Left, LevelType.Hard2);
			break;
		}
		fingers.Add (f);
		cam.NextPoint (actualPos - nextFingerDistance,  fingers[fingers.Count-2].GetComponent<FingerController> ().posX);
		prevX = f.transform.position.x;
	}

	public void ClearScene(){
		cam.ToStartPosition();
		foreach(GameObject o in fingers){
			Destroy (o);
		}
		fingers.Clear ();
		ClearVars ();
		Destroy (instanceSpinner);
	}

	public void Continue(){
		if (HZIncentivizedAd.IsAvailable()) {
			HeyzappAds.ShowRewardedVideoAd ();
			GameObject o = fingers [fingers.Count - 2];
			Destroy (instanceSpinner);
			instanceSpinner = Instantiate (spinner);
			instanceSpinner.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, 0));
			instanceSpinner.transform.position = new Vector3 (o.transform.position.x, 0, o.transform.position.z);
			instanceSpinner.GetComponent<SpinnerController> ().StartRotate (rotateSpeed);

			gsc.HideGameOverScreen ();
            
		}
	}

	public void GameOver(){
		Settings.TotalGames++;
		gsc.OpenGameOverScreen ();
	}

	public void GameOverButton(){
		//Application.LoadLevel ("main");
		ClearScene();
		SceneController.instance.gameScreen.Hide();
		SceneController.instance.gameScreen.BackToMainMenuAds ();
		//SceneController.instance.mainScreen.Show ();
	}

	public void Jump(float x){
		SoundManager.instance.Jump ();
		dir = new Vector3(x * 3.0f, dir.y, dir.z);
		instanceSpinner.GetComponent<SpinnerController> ().Jump (dir);
	}

	public enum LevelType{
		Easy,
		Default,
		Hard,
		Hard1,
		Hard2
	}
}


