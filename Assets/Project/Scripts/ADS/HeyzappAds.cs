﻿using Heyzap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeyzappAds : MonoBehaviour {

    public static readonly string VIDEO_ADS_COUNT = "videoAdsC";
    public static readonly string INTERSTITIAL_ADS_COUNT = "InterstitialAdsC";
    public static readonly string REVARDED_ADS_COUNT = "revardedAdsC";

    void Start()
    {
		HeyzapAds.Start("004f95322e0abbd38a67f81d8c2e188e", HeyzapAds.FLAG_NO_OPTIONS);

        SetCallbacks();
        ShowBanner();
        HZIncentivizedAd.Fetch();
        HZVideoAd.Fetch();
        HZInterstitialAd.Fetch();
    }

    void SetCallbacks()
    {
        HZInterstitialAd.AdDisplayListener listener = delegate (string adState, string adTag) {
            if (adState.Equals("show"))
            {
                InterstitialAdsCount++;
                MyAnalytic.ShowAdsStatic("Interstitial", InterstitialAdsCount);
            }
            if (adState.Equals("click"))
            {
                MyAnalytic.CustomEventStatic("ADS", "Interstitial_click", "");
            }
            if (adState.Equals("failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Interstitial_failed", "");
            }
            if (adState.Equals("available"))
            {
            }
            if (adState.Equals("fetch_failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Interstitial_fetch_failed", "");
            }
        };
        HZInterstitialAd.SetDisplayListener(listener);


        HZVideoAd.AdDisplayListener listenerVideo = delegate (string adState, string adTag) {
            if (adState.Equals("show"))
            {
                VideoAdsCount++;
                MyAnalytic.ShowAdsStatic("Video", VideoAdsCount);
            }
            if (adState.Equals("click"))
            {
                MyAnalytic.CustomEventStatic("ADS", "Video_click", "");
            }
            if (adState.Equals("failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Video_failed", "");
            }
            if (adState.Equals("available"))
            {
            }
            if (adState.Equals("fetch_failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Video_fetch_failed", "");
            }
        };
        HZVideoAd.SetDisplayListener(listenerVideo);

        HZIncentivizedAd.AdDisplayListener listenerIncentivized = delegate (string adState, string adTag) {
            if (adState.Equals("show"))
            {
                VideoAdsCount++;
                MyAnalytic.ShowAdsStatic("Rewarded", VideoAdsCount);
            }
            if (adState.Equals("click"))
            {
                MyAnalytic.CustomEventStatic("ADS", "Rewarded_click", "");
            }
            if (adState.Equals("failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Rewarded_failed", "");
            }
            if (adState.Equals("available"))
            {
            }
            if (adState.Equals("fetch_failed"))
            {
                MyAnalytic.CustomEventStatic("ADS_fails", "Rewarded_fetch_failed", "");
            }
        };
        HZIncentivizedAd.SetDisplayListener(listenerIncentivized);
    }

    void ShowBanner()
    {
        if (!Settings.NOADS)
        {
            HZBannerShowOptions showOptions = new HZBannerShowOptions();
            showOptions.Position = HZBannerShowOptions.POSITION_BOTTOM;
            HZBannerAd.AdDisplayListener listener = delegate (string adState, string adTag) {                
                if (adState == "click")
                {
                    MyAnalytic.CustomEventStatic("ADS","Banner_click","");
                }
            };
            HZBannerAd.SetDisplayListener(listener);
            HZBannerAd.ShowWithOptions(showOptions);
            //StartCoroutine(adsShow());        
        }
    }

    public static void ShowInterstitialAd()
    {
        if (HZInterstitialAd.IsAvailable())
        {            
            HZInterstitialAd.Show();
            HZInterstitialAd.Fetch();
        }
    }

    public static int InterstitialAdsCount
    {
        get { return PlayerPrefs.GetInt(INTERSTITIAL_ADS_COUNT); }
        set { PlayerPrefs.SetInt(INTERSTITIAL_ADS_COUNT, value); }
    }

    public static void ShowRewardedVideoAd()
    {
        if (HZIncentivizedAd.IsAvailable())
        {
            HZIncentivizedAd.Show();
            HZIncentivizedAd.Fetch();
        }
    }

    public static void ShowVideoAd()
    {
        if (HZVideoAd.IsAvailable())
        {
            HZVideoAd.Show();            
            HZVideoAd.Fetch();
        }
    }

    public static int VideoAdsCount
    {
        get { return PlayerPrefs.GetInt(VIDEO_ADS_COUNT) ; }
        set { PlayerPrefs.SetInt(VIDEO_ADS_COUNT, value); }
    }

    public void OpenAdsTest()
    {
        HeyzapAds.ShowMediationTestSuite();
    }
}
