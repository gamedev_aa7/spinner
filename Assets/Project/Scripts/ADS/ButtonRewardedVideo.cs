﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Heyzap;

public class ButtonRewardedVideo : MonoBehaviour {	

	public GameObject btn;

	void Start () {
		HZIncentivizedAd.Fetch ();
		StartCoroutine(Checker());
	}

	IEnumerator Checker()
	{
		while (true)
		{            
			if (HZIncentivizedAd.IsAvailable())
			{
				gameObject.SetActive(true);
			}
			else
			{
				gameObject.SetActive(false);
			}
			yield return new WaitForSeconds(1);
		}
	}
}
