﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Heyzap;

public class ButtonVideoShow : MonoBehaviour {

    public GameObject btn;
    
	void Start () {
        StartCoroutine(Checker());
	}

    IEnumerator Checker()
    {
        while (true)
        {            
            if (HZVideoAd.IsAvailable())
            {
                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(1);
        }
    }
}
