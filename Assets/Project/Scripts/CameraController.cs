﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float speed;
	public float dlt;
	private Vector3 newPos;
	private Vector3 startPos;

	void Start(){
		startPos = newPos = transform.position;
	}

	void Update () {
		transform.position = Vector3.Lerp (transform.position, newPos , speed * Time.deltaTime);
	}

	public void NextPoint(float distance, float posX){
		newPos = new Vector3 (posX,transform.position.y, distance - System.Math.Abs(dlt));
	}

	public void ToStartPosition(){
		transform.position = startPos;
	}
}
