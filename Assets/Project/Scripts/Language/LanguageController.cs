﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Frase", menuName = "Frases/Frase Language", order = 1)]
[System.Serializable]
public class LanguageController : ScriptableObject {
   
    public string defaultStr;
    public StringValue[] lines;


    public string Value
    {
        get {
            SystemLanguage l = Application.systemLanguage;
			if (l == SystemLanguage.Russian) {
				return defaultStr;
			}
            foreach (StringValue s in lines)
            {
                if(s.language == l)
                {
                    return s.val;
                }
            }
			if (lines.Length > 0) {
				return lines [0].val;
			}
            return defaultStr;
        }
    }
}

[System.Serializable]
public struct StringValue
{
    public string val;
    public SystemLanguage language;
}