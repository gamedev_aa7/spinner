﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeImage : MonoBehaviour {

	public Image img;
	public LanguageImgController db;

	void Start()
	{
		img.sprite = db.Value;
	}
}
