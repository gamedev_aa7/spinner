﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Image", menuName = "Frases/Frase img Language", order = 1)]
[System.Serializable]
public class LanguageImgController : ScriptableObject {

	public Sprite defaultImg;
	public ImgValue[] lines;


	public Sprite Value
	{
		get {
			SystemLanguage l = Application.systemLanguage;
			if (l == SystemLanguage.Russian) {
				return defaultImg;
			}
			foreach (ImgValue s in lines)
			{
				if(s.language == l)
				{
					return s.val;
				}
			}
			if (lines.Length > 0) {
				return lines [0].val;
			}
			return defaultImg;
		}
	}
}

[System.Serializable]
public struct ImgValue
{
	public Sprite val;
	public SystemLanguage language;
}