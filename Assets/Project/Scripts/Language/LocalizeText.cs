﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizeText : MonoBehaviour {

    public Text txt;
    public LanguageController db;

    void Awake()
    {
        txt.text = db.Value;
    }

    void Start()
    {
        txt.text = db.Value;
    }
}
