﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandController : MonoBehaviour {

    public GameObject swController;
	public SkinUIController ui;
    public float speed;
	public InitSkins initSk;
	public GameObject focus;
	public float forceCoff = 100;
	public float forceLerpSpeed = 2.0f;
	public float moveCoff = 100;
    private Vector3 pos = new Vector3(0,0,0);
    bool touch = false;
    public float force = 0;

    void Start()
    {
        swController.GetComponent<SwipeManager>().AddStartTouch(() => { touch = true; force = 0; });
        swController.GetComponent<SwipeManager>().AddFinishTouch(() => { touch = false; });
        swController.GetComponent<SwipeManager>().AddMoveTouch(MoveMenu);
        swController.GetComponent<SwipeManager>().AddEndMoveTouch(ForceMenu);
    }

    public void Unactivate()
    {
        foreach (SkinContainerController s in FindObjectsOfType<SkinContainerController>())
        {
            s.isActive = false;
        }
    }

    public void SetFocus(GameObject f)
    {
        ui.GetComponent<SkinUIController>().InitSkin(gameObject, f);
        Vector3 p = transform.position;
		float fv = (f.GetComponent<SkinContainerController>().skin.number * initSk.widthDelta);
        p.x -= fv;
        pos = p;
    }

    public void moveToFocused()
    {
        focus.transform.position = pos;
    }

    void Update()
    {
		if (force != 0) {
			force = Vector2.MoveTowards (new Vector2 (force, force), new Vector2 (0, 0), Time.deltaTime * forceLerpSpeed).x;
			MoveMenu (force);            
		}
		else if (touch == false)
        {
			//if (force == 0)
			{
				FindClosest();
			}
            focus.transform.position = Vector3.Lerp(focus.transform.position, pos, Time.deltaTime * speed);
        }
    }

    private void FindClosest()
    {
        float length = float.MaxValue;
        GameObject o = null;
        foreach(SkinContainerController sc in FindObjectsOfType<SkinContainerController>()){
            float dlt = (transform.position - sc.gameObject.transform.position).magnitude;
            if (dlt <= length)
            {
                length = dlt;
                o = sc.gameObject;
            }
        }

        SetFocus(o);
    }

    public void MoveMenu(float f)
    {
		//touch = true; 
		focus.transform.position += new Vector3(-f * moveCoff,0,0);
    }

    public void ForceMenu(float f)
    {
        f *= forceCoff;
        force = f;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<SkinContainerController>())
        {
            SetFocus(other.gameObject);
        }
    }
}
