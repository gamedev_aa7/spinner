﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitSkins : MonoBehaviour {

	public SkinsController skinController;
	public GameObject containerPF;
	public float widthDelta;
	public Transform startPos;
	public StandController st;

	void Start () {
		transform.position = startPos.position;
	}

	public void Init(){
		foreach(Transform t in gameObject.transform){
			Destroy (t.gameObject);
		}
		GenerateSkins ();
		//Vector3 pos = transform.position;
		//pos.z = 1000;
		//transform.position = pos;
	}

	private void GenerateSkins(){
		Vector3 lastPos = new Vector3(0,0,0);

		int activeId = skinController.GetComponent<SkinsController>().GetActiveIndex();
		foreach(Skin sk in skinController.skins)
		{
			GameObject container = Instantiate(containerPF);
			container.transform.parent = transform;
			container.transform.localPosition = lastPos;
			lastPos.x += widthDelta;            
			container.GetComponent<SkinContainerController>().InitSkin(sk);
			if(sk.number == activeId){
				container.GetComponent<SkinContainerController>().isActive = true;
				st.SetFocus (container);
			}
			if (skinController.GetComponent<SkinsController>().CheckPurchased(sk.number) == true)
			{
				container.GetComponent<SkinContainerController>().isBuy = true;
			}
		}
	}
}
