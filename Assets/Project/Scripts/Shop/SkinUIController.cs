﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinUIController : MonoBehaviour {

    public GameObject standController;
	public SkinsController sc;
    public Text label;
    public Text cost;
	public GameObject playButton;
	public GameObject buyButton;
    public GameObject activateButton;
    private GameObject skin;

	public Sprite costCoins;
	public Sprite costFacebook;
	public Sprite costTwitter;

    public void InitSkin(GameObject o, GameObject s)
    {
        skin = s;
        standController = o;
        label.text = skin.GetComponent<SkinContainerController>().skin.name;
        cost.text = skin.GetComponent<SkinContainerController>().skin.cost.ToString();
        CheckStates();
    }

    private void CheckStates()
    {
        if (skin.GetComponent<SkinContainerController>().isActive == true)
        {
			playButton.SetActive (true);
            buyButton.SetActive(false);
            activateButton.SetActive(false);
			cost.enabled = false;
        }
        else if (skin.GetComponent<SkinContainerController>().isBuy == true)
        {
			playButton.SetActive (false);
            buyButton.SetActive(false);
            activateButton.SetActive(true);
			cost.enabled = false;
        }
        else
        {
			playButton.SetActive (false);
            buyButton.SetActive(true);
            activateButton.SetActive(false);
			cost.enabled = true;

			switch(skin.GetComponent<SkinContainerController>().skin.costType){
			case CostType.Coins:
				buyButton.GetComponent<Button> ().GetComponent<Image> ().sprite = costCoins;
				break;
			case CostType.Facebook:
				if (!SocialNetworks.FacebookState) {
					buyButton.GetComponent<Button> ().GetComponent<Image> ().sprite = costFacebook;
				} else {
					//Buy ();
				}
				break;
			case CostType.Twitter:
				if (!SocialNetworks.TwitterState) {
					buyButton.GetComponent<Button> ().GetComponent<Image> ().sprite = costTwitter;
				} else {
					//Buy ();
				}
				break;
			}
        }
    }

    public void Buy()
    {
        if (!sc.CheckPurchased(skin.GetComponent<SkinContainerController>().skin.number))
        {
			switch(skin.GetComponent<SkinContainerController>().skin.costType){
			case CostType.Coins:
				if (skin.GetComponent<SkinContainerController> ().skin.cost <= Settings.Gems) {
					Settings.Gems -=skin.GetComponent<SkinContainerController> ().skin.cost;
					sc.BuySkin (skin.GetComponent<SkinContainerController> ().skin.number);
					skin.GetComponent<SkinContainerController> ().isBuy = true;
				} else {
					SceneController.instance.skinsScreen.Hide ();
					SceneController.instance.shopScreen.OpenShop (SceneController.instance.mainScreen.gameObject);
				}
				break;
			case CostType.Facebook:
				if (!SocialNetworks.FacebookState) {
					SocialNetworks.FacebookState = true;
					skin.GetComponent<SkinContainerController> ().isBuy = true;
					sc.BuySkin (skin.GetComponent<SkinContainerController> ().skin.number);

					Application.OpenURL (SocialNetworks.instance.facebook);
				}
				break;
			case CostType.Twitter:
				if (!SocialNetworks.TwitterState) {
					SocialNetworks.TwitterState = true;
					sc.BuySkin (skin.GetComponent<SkinContainerController> ().skin.number);
					skin.GetComponent<SkinContainerController> ().isBuy = true;

					Application.OpenURL (SocialNetworks.instance.twitter);
				}
				break;
			}
        }     

		CheckStates ();
    }

	public void Play(){
		SceneController.instance.gameScreen.Show ();
		SceneController.instance.skinsScreen.Hide ();
	}

    public void Activate()
    {
        if (sc.CheckPurchased(skin.GetComponent<SkinContainerController>().skin.number) == true)
        {
            standController.GetComponent<StandController>().Unactivate();
            sc.SetActive(skin.GetComponent<SkinContainerController>().skin.number);    
			skin.GetComponent<SkinContainerController> ().isActive = true;
        }
		CheckStates ();
    }

    public void UnActivate()
    {
        if (sc.CheckPurchased(skin.GetComponent<SkinContainerController>().skin.number) == true)
        {
            buyButton.SetActive(false);
            activateButton.SetActive(true);
			playButton.SetActive (false);
			cost.enabled = false;
        }
        else
        {
			playButton.SetActive (false);
            buyButton.SetActive(true);
            activateButton.SetActive(false);
			cost.enabled = false;
        }
		skin.GetComponent<SkinContainerController> ().isActive = false;
    }
}
