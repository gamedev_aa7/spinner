﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PreloaderController : MonoBehaviour {

	public int sceneId;

	public Image img;
	public Text progressText;
    public GameObject loadingFinish;

	void Start () {
		StartCoroutine (AsycnLoader());
	}

	IEnumerator AsycnLoader(){
		AsyncOperation o = SceneManager.LoadSceneAsync (sceneId);
		while (true) {
			float progress = o.progress / 0.9f;
			img.fillAmount = progress;
			progressText.text = string.Format ("{0:0}%",progress*100);
            if (progress >= 0.94f)
            {
                loadingFinish.SetActive(true);
                break;
            }
			yield return null;
		}
	}
}
