﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Heyzap;

public class SkinsShopScreen : ScreenController {

	public GameObject shopSkins;
	private GameObject prevScreen;
	public InitSkins ins;

	void Update () {
		UpdateText();
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Back();
		}
	}

	public void OpenShop(GameObject o)
	{
		prevScreen = o;
		prevScreen.GetComponent<ScreenController>().Hide();
		Show();
		shopSkins.SetActive (true);
		ins.Init ();
		//Analytics.SceneOpen("Shop", o.name);
	}

	public void VideoAds(){
		if (HZIncentivizedAd.IsAvailable ()) {
			HeyzappAds.ShowRewardedVideoAd ();
			Settings.Gems += Settings.instance.coinsForVideo;
		}
	}

	public void OpenCoinShop(){
		SceneController.instance.shopScreen.OpenShop (SceneController.instance.mainScreen.gameObject);
		Hide ();
	}

	public override void Hide(){
		gameObject.SetActive (false);
		shopSkins.SetActive (false);
	}

	public override void Show(){
        base.Show();
	}

	public void Back()
	{
		Hide();
		Debug.Log(prevScreen.name + "Open");
		prevScreen.GetComponent<ScreenController>().Show();
	}
}
