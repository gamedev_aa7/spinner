﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

	public static SceneController instance;

	public MainScreenController mainScreen;
	public GameScreenController gameScreen;
	public ShopScreenController shopScreen;
	public SkinsShopScreen skinsScreen;

	void Start () {
		instance = gameObject.GetComponent<SceneController> ();
	}
}
