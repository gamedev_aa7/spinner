﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScreenController : ScreenController {

	public GameObject gameScreen;
    public GameObject exitDialog;

	void Start () {
        InitText();
        //exitDialog.SetActive(false);
        //Analytics.GameStarted();
        //Analytics.SceneOpen("Main", "none");
	}
		
	void Update () {
        UpdateText();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //ShowExitDialog();
        }
    }

    public override void Show()
    {
        base.Show();
        MyAnalytic.OpenSceneStatic("MainScene");
    }

    private void ShowExitDialog()
    {
        if (!exitDialog.activeSelf)
        {
            exitDialog.SetActive(true);
        }
    }

    public void StartClick()
    {
        //Analytics.SceneOpen("Game", "Main");
        gameScreen.GetComponent<GameScreenController>().Show();
        Hide();        
    }
}
