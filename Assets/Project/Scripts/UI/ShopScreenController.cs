﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Heyzap;

public class ShopScreenController : ScreenController {
    
    private GameObject prevScreen;

	void Update () {
        UpdateText();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Back();
        }
    }

    public void OpenShop(GameObject o)
    {
        Show();
        prevScreen = o;
        prevScreen.GetComponent<ScreenController>().Hide();
        //Analytics.SceneOpen("Shop", o.name);
    }

	public override void Hide ()
	{
		gameObject.SetActive (false);
	}

	public override void Show(){
        base.Show();
	}

	public void OpenSkinShop(){
		Hide ();
		SceneController.instance.skinsScreen.OpenShop (SceneController.instance.mainScreen.gameObject);
	}

    public void Back()
    {
        Hide();
        Debug.Log(prevScreen.name + "Open");
        prevScreen.GetComponent<ScreenController>().Show();
    }

    public void ButtonConverter()
    {
        if (Settings.Gems >= 1)
        {
            Settings.Gems--;
            Settings.Coins += 100;
        }
    }

    public void ButtonGemForVideo()
    {
        //if (HZIncentivizedAd.IsAvailable())
        {
            //HeyzappAds.ShowRewardedVideoAd();
            Settings.Gems++;
        }
    }

    public void ButtonPlusGems(int c)
    {
        Settings.Gems+= c;
        //soundManager.PlusGems();
    }
}
