﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Heyzap;

public class GameScreenController : ScreenController {

    public GameObject mainMenu;
    public GameObject shopScreen;
	public GameObject deadScreen;    

	public Text bestScore;
	public LanguageController bestFrase;
	public GameController gc;
	
	void Start () {
	}
	
	void Update () {
		UpdateText ();
    }

	public override void Hide()
	{
		SwipeManager.gameState = false;
		gc.ClearScene ();
		gameObject.SetActive(false);
	}

    public override void Show()
    {
        base.Show();
        deadScreen.SetActive(false);
        SwipeManager.gameState = true;
        gc.StartGame();
    }

    public void OpenGameOverScreen(){
		bestScore.text = bestFrase.Value+" "+Settings.BestCoins;
		deadScreen.SetActive (true);
		SwipeManager.gameState = false;
	}

	public void HideGameOverScreen(){
		deadScreen.SetActive (false);
		SwipeManager.gameState = true;
	}

	public void BackToMainMenuAds()
	{
		if (!Settings.NOADS) {
			if (Settings.TotalGames % Settings.instance.adsFrequency == 0) {
				Settings.SHOWADS = true;
			}
			if (Settings.SHOWADS) {
				if (HZVideoAd.IsAvailable ()) {
					HeyzappAds.ShowVideoAd ();
					Settings.SHOWADS = false;
				}
			}
		}

		BackToMainMenu ();
	}

    public void BackToMainMenu()
    {
		SwipeManager.gameState = false;
        //Analytics.SceneOpen("Main", "Game");
		gc.ClearScene();
        Hide();
        mainMenu.GetComponent<MainScreenController>().Show();
    }
}
