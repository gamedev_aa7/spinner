﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ScreenController : MonoBehaviour {

    public Text coins;
    private int c = 0;
    public Text gems;
    private int g = 0;

    private void Awake()
    {
        InitText();
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
        MyAnalytic.OpenSceneStatic(this.GetType().ToString());
        Debug.Log(this.GetType().ToString());
    }

    public void UpdateText()
    {
        UpdateText(coins, Settings.Coins, ref c);
        UpdateText(gems, Settings.Gems, ref g);
    }

    public void InitText()
    {
        c = -1;
        g = -1;
        UpdateText();
    }

    protected void UpdateText(Text t, int actual, ref int prev)
    {
        if (actual != prev)
        {
            t.text = actual+"";
            prev = actual;
        }
    }
}
