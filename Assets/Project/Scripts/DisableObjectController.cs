﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObjectController : MonoBehaviour {

	public SystemLanguage[] language;

	void Start () {
		foreach(SystemLanguage l in language){
			if (Application.systemLanguage != l) {
				gameObject.SetActive (false);
			}
		}
	}
}
