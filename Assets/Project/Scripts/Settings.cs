﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Settings : MonoBehaviour {
    
    [Range(1, 100)]
    public int adsFrequency = 0;
    [Range(1, 100)]
    public int coinsForVideo = 10;

	public static Settings instance;

    void Start()
    {
		instance = gameObject.GetComponent<Settings> ();
        CoinsForVideo = coinsForVideo;
    }

	public static bool SHOWADS
	{
		get { return PlayerPrefs.GetInt("showads", 0) > 0 ? true : false; }
		set
		{
			if (value)
			{
				PlayerPrefs.SetInt("showads", 1);
			}
			else
			{
				PlayerPrefs.SetInt("showads", 0);
			}
		}
	}

    public static int Coins
    {
        get { return PlayerPrefs.GetInt("coins", 0); }
        set
        {
            if (value < 0)
            {
                PlayerPrefs.SetInt("coins", 0);
            }
            else
            {
				TotalLevels++;
				if (value > BestCoins) {
					BestCoins = value;
				}
                PlayerPrefs.SetInt("coins", value);
				ServiceController.instance.SetTop (value);
				#if UNITY_ANDROID && !UNITY_EDITOR
				ServiceController.service.GetComponent<AndroidService> ().AchiveLevels (value);
				#endif
            }
        }
    }

	public static int TotalGames
	{
		get { return PlayerPrefs.GetInt("totalgames", 0); }
		set
		{
			if (value < 0)
			{
				PlayerPrefs.SetInt("totalgames", 0);
			}
			else
			{
				PlayerPrefs.SetInt("totalgames", value);
			}
		}
	}

	public static int BestCoins
	{
		get { return PlayerPrefs.GetInt("bestcoins", 0); }
		set
		{
			if (value < 0)
			{
				PlayerPrefs.SetInt("bestcoins", 0);
			}
			else
			{
				PlayerPrefs.SetInt("bestcoins", value);
			}
		}
	}

	public static int TotalLevels
	{
		get { return PlayerPrefs.GetInt("totalLevels", 0); }
		set
		{
			if (value < 0)
			{
				PlayerPrefs.SetInt("totalLevels", 0);
			}
			else
			{
				if (value % 10 == 0) {
					Gems++;
				}
				PlayerPrefs.SetInt("totalLevels", value);
			}
		}
	}
	
    public static int CoinsForVideo
    {
        get { return PlayerPrefs.GetInt("coinsADS", 10); }
        set { PlayerPrefs.SetInt("coinsADS", value); }
    }
	
    public static int Gems
    {
        get { return PlayerPrefs.GetInt("gems", 0); }
        set
        {
            if (value < 0)
            {
                PlayerPrefs.SetInt("gems", 0);
            }
            else
            {
                PlayerPrefs.SetInt("gems", value);
            }
        }
    }

    public static bool NOADS
    {
        get { return PlayerPrefs.GetInt("noads", 0) > 0 ? true : false; }
        set
        {
            if (value)
			{
                PlayerPrefs.SetInt("noads", 1);
            }
            else
            {
                PlayerPrefs.SetInt("noads", 0);
            }
        }
    }

    public static bool Sounds
    {
        get { return PlayerPrefs.GetInt("sounds", 1) > 0 ? true : false; }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("sounds", 1);
            }
            else
            {
                PlayerPrefs.SetInt("sounds", 0);
            }
        }
    }

    public static bool Music
    {
        get { return PlayerPrefs.GetInt("music", 1) > 0 ? true : false; }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("music", 1);
            }
            else
            {
                PlayerPrefs.SetInt("music", 0);
            }
        }
    }

    public static bool RateState
    {
        get { return PlayerPrefs.GetInt("rateState", 0) > 0 ? true : false; }
        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("rateState", 1);
            }
            else
            {
                PlayerPrefs.SetInt("rateState", 0);
            }
        }
    }
}