﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    static GameObject singletone = null;
    AudioSource a;
    
    void Start () {
        if (singletone == null)
        {
            singletone = gameObject;
            a = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(singletone);
        check();
    }

    public void Mute()
    {
        a.volume = 0;
        Settings.Music = false;
        Debug.Log("Music Mute");
    }

    public void UnMute()
    {
        a.volume = 1;
        Settings.Music = true;
        Debug.Log("Music unmute");
    }

    private void check()
    {
        if (Settings.Music)
        {
            UnMute();
        }
        else
        {
            Mute();
        }
    }
}
