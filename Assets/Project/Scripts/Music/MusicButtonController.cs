﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButtonController : MonoBehaviour
{
    public Sprite on;
    public Sprite off;    

    private Image i;

    private void Awake()
    {
        i = gameObject.GetComponent<Image>();
        CheckImage();
    }

    public void Click()
    {
        Debug.Log("Music click");
        if (Settings.Music)
        {
            GameObject.FindObjectOfType<MusicManager>().Mute();
        }
        else
        {
            GameObject.FindObjectOfType<MusicManager>().UnMute();
        }
        CheckImage();
    }

    private void CheckImage()
    {
        if (Settings.Music)
        {
            i.sprite = on;
        }
        else
        {
            i.sprite = off;
        }
    }
}