﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

public class InAppPurchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    public static string IDNonConsumableNoAds = "noads";
    public static string IDConsumableCoins500 = "500coin";
    public static string IDConsumableCoins1000 = "1000coin";
    public static string IDConsumableCoins1500 = "1500coin";
	public static string IDConsumableCoins2000 = "2000coin";
	public static string IDConsumableCoins3500noads = "3500noads";


    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {

        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //builder.AddProduct(kProductIDConsumable, ProductType.Consumable);
        builder.AddProduct(IDNonConsumableNoAds, ProductType.NonConsumable);
        builder.AddProduct(IDConsumableCoins500, ProductType.Consumable);
        builder.AddProduct(IDConsumableCoins1000, ProductType.Consumable);
        builder.AddProduct(IDConsumableCoins1500, ProductType.Consumable);
        builder.AddProduct(IDConsumableCoins2000, ProductType.Consumable);
        builder.AddProduct(IDConsumableCoins3500noads, ProductType.Consumable);

        //builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
        //    { kProductNameAppleSubscription, AppleAppStore.Name },
        //    { kProductNameGooglePlaySubscription, GooglePlay.Name },
        //});

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //public void BuyConsumable()
    //{
    //    BuyProductID(kProductIDConsumable);
    //}


    public void BuyNoAds()
    {
        if (!Settings.NOADS)
        {
            BuyProductID(IDNonConsumableNoAds);
        }
    }

    public void Buy500Coins()
    {
        BuyProductID(IDConsumableCoins500);
    }

	public void Buy1000Coins()
	{
		BuyProductID(IDConsumableCoins1000);
	}

	public void Buy2000Coins()
	{
		BuyProductID(IDConsumableCoins2000);
	}

    public void Buy1500Coins()
    {
        BuyProductID(IDConsumableCoins1500);
    }

    public void Buy3500noadsCoins()
    {
        BuyProductID(IDConsumableCoins3500noads);
    }    

    //public void BuySubscription()
    //{
    //    BuyProductID(kProductIDSubscription);
    //}


    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));

                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<UnityEngine.Purchasing.IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                if (result)
                {
                    Settings.NOADS = true;
                }
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, IDNonConsumableNoAds, StringComparison.Ordinal))
        {
            Settings.NOADS = true;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, IDConsumableCoins500, StringComparison.Ordinal))
        {
            Settings.Gems += 500;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, IDConsumableCoins1000, StringComparison.Ordinal))
        {
            Settings.Gems += 1000;
        }
		else if (String.Equals(args.purchasedProduct.definition.id, IDConsumableCoins1500, StringComparison.Ordinal))
		{
			Settings.Gems += 1500;
		}
		else if (String.Equals(args.purchasedProduct.definition.id, IDConsumableCoins2000, StringComparison.Ordinal))
		{
			Settings.Gems += 2000;
		}
        else if (String.Equals(args.purchasedProduct.definition.id, IDConsumableCoins3500noads, StringComparison.Ordinal))
        {
            Settings.Gems += 3500;
            Settings.NOADS = true;
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}