﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinShopController : MonoBehaviour {

	public void BuyCoins(int c){
		Settings.Coins += c;
	}

	public void BuyNoAds(){
		Settings.NOADS = true;
	}
}
