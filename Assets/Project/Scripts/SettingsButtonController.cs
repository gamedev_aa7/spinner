﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsButtonController : MonoBehaviour {

    public GameObject animPanel;
    private bool openState = false;
    private Animation a;
    private int clickCount = 0;
    private float timeDelay;    

    private void Start()
    {
        a = animPanel.GetComponent<Animation>();
        timeDelay = a.clip.length * 0.6f;
    }

    public void Click()
    {
        if (clickCount < 1)
        {
            clickCount++;
            if (!openState)
            {
                Open();
            }
            else
            {
                Close();
            }
            StartCoroutine(clickDelay());
        }
    }

    private void Open()
    {
        if (a.isPlaying)
        {
            a["HideSettings"].speed = -1;
            a.Play("HideSettings");            
        }
        else
        {            
            a["ShowSettings"].speed = 1;
            a.Play("ShowSettings");            
        }
        openState = true;
    }

    private void Close()
    {
        if (a.isPlaying)
        {
            a["ShowSettings"].speed = -1;
            a.Play("ShowSettings");
        }
        else
        {
            a["HideSettings"].speed = 1;
            a.Play("HideSettings");            
        }
        openState = false;
    }

    IEnumerator clickDelay()
    {
        float t = 0;
        while (t < timeDelay)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            t += Time.deltaTime;
        }
        clickCount = 0;
    }
}
