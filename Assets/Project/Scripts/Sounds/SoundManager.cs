﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    AudioSource a;
	public AudioClip[] jump;
    public AudioClip[] clip;
    public AudioClip[] gemPlus;
    public AudioClip[] screnShoot;

	public static SoundManager instance;

    private void Awake()
    {
        a = gameObject.GetComponent<AudioSource>();
        check();
    }

    void Start () {        
		instance = gameObject.GetComponent<SoundManager> ();
	}

    public void Mute()
    {
        a.volume = 0;
        Settings.Sounds = false;
    }

    public void UnMute()
    {
        a.volume = 1;
        Settings.Sounds = true;
    }

    public void Click()
    {
        a.PlayOneShot(clip[0]);
    }

	public void Jump()
	{
		if (jump.Length > 0)
		{
			a.PlayOneShot(jump[UnityEngine.Random.Range(0, jump.Length)]);
		}
	}

    public void PlusGems()
    {
        if (gemPlus.Length > 0)
        {
            a.PlayOneShot(gemPlus[UnityEngine.Random.Range(0, gemPlus.Length)]);
        }
    }

    public void ScrenShoot()
    {
        if (screnShoot.Length > 0)
        {
            a.PlayOneShot(screnShoot[UnityEngine.Random.Range(0, screnShoot.Length)]);
        }
    }

    private void check()
    {
        if (Settings.Sounds)
        {
            UnMute();
        }
        else
        {
            Mute();
        }
    }
}
