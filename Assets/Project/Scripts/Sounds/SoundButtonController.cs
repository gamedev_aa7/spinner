﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButtonController : MonoBehaviour {

    public Sprite on;
    public Sprite off;

    private SoundManager soundManager;

    private Image i;

    private void Awake()
    {
        soundManager = GameObject.FindObjectOfType<SoundManager>();
        i = gameObject.GetComponent<Image>();
        CheckImage();
    }

    public void Click()
    {
        if (Settings.Sounds)
        {
            soundManager.Mute();
        }
        else
        {
            soundManager.UnMute();
        }
        CheckImage();
    }

    private void CheckImage()
    {
        if (Settings.Sounds)
        {
            i.sprite = on;
        }
        else
        {
            i.sprite = off;
        }
    }
}
