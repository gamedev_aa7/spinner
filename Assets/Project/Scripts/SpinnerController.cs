﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerController : MonoBehaviour {

	[Header("Swipe settings")]
	public Vector2 deltaX;
	public Vector2 deltaY;
	public Vector2 deltaZ;

	public GameObject body;
	public GameObject image;

	private Vector3 rotateSpeed;
	private bool rotateState = false;
	public GameController contr;

	private bool jumpState = false;
	private Rigidbody rb;

	void Start(){
		rb = GetComponent<Rigidbody> ();
		GameObject o = GameObject.Find ("GameController");
		if(o!= null){
			contr = o.GetComponent<GameController>();
		}
	}

	public void StartRotate(float speed){
		rotateState = false;
		new WaitForSeconds (Time.deltaTime*2);
		rotateSpeed = new Vector3 (0,speed,0);
		rotateState = true;
		StartCoroutine (rotation());
	}

	private void Rotate(){
		image.transform.Rotate (rotateSpeed);
	}

	IEnumerator rotation(){
		while (rotateState) {
			Rotate ();
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "floor") {
			contr.GameOver ();
		}
		if (collision.gameObject.tag == "finger") {
			jumpState = false;
			FingerController f = collision.gameObject.GetComponent<FingerController> ();
			collision.gameObject.GetComponent<FingerController> ().CollisionEvent ();
			float z = collision.gameObject.transform.position.z;
			transform.position = new Vector3 (transform.position.x, transform.position.y, z);
			if (f.isActive) {
				contr.NextFinger ();
				f.isActive = false;
			}
		}
	}

	public void Jump(Vector3 dir){
		dir = CheckDelta (dir);
		if (!jumpState) {
			jumpState = true;
			rb.AddForce (dir, ForceMode.Impulse);
		}
	}

	private Vector3 CheckDelta(Vector3 v){
		return new Vector3 (CheckDelta(deltaX, v.x), CheckDelta(deltaY, v.y), CheckDelta(deltaZ, v.z));
	}

	private float CheckDelta(Vector2 dlt, float val){
		if (val < dlt.x) {
			return dlt.x;
		}
		if (val > dlt.y) {
			return dlt.y;
		}
		return val;
	}
}
