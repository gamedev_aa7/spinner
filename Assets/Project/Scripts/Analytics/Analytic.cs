﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Analytic : MonoBehaviour {

    [HideInInspector]
    public string analyticName;

    public virtual void ButtonClick(string btnKey) { }
    public virtual void OpenScene(string sceneName) { }
    public virtual void ShowAds(string adsType, int value) { }
    public virtual void CustomEvent(string eventType, string eventName, string value) { }
    public virtual void CustomEvent(string eventType) { }
}
