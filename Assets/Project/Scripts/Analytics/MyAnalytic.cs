﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAnalytic : Analytic {
    
    public Analytic analytic;

    public static MyAnalytic Instance
    {
        get
        {
            return instance;
        }
    }
    private static MyAnalytic instance = null;

    void Start()
    {
        if (instance == null)
        {
            instance = gameObject.GetComponent<MyAnalytic>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public override void ButtonClick(string btnKey)
    {
        analytic.ButtonClick(btnKey);
    }

    public override void OpenScene(string sceneName)
    {
        analytic.OpenScene(sceneName);
    }

    public static void OpenSceneStatic(string sceneName)
    {
        Instance.OpenScene(sceneName);
    }

    public override void CustomEvent(string eventType, string eventName, string value)
    {
        analytic.CustomEvent(eventType, eventName, value);
    }

    public static void CustomEventStatic(string eventType, string eventName, string value)
    {
        Instance.CustomEvent(eventType, eventName, value);
    }

    public override void CustomEvent(string eventType)
    {
        analytic.CustomEvent(eventType);
    }

    public static void CustomEventStatic(string eventType)
    {
        Instance.CustomEvent(eventType);
    }

    public override void ShowAds(string eventType, int value)
    {
        analytic.ShowAds(eventType, value);
    }

    public static void ShowAdsStatic(string eventType, int value)
    {
        Instance.ShowAds(eventType, value);
    }
}
