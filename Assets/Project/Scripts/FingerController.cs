﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerController : MonoBehaviour {

	public GameObject img;
	public ParticleSystem effect;
	public bool isActive = true;
	public Vector2 deltaX;
	public float posX = 0;
	public float speedInit;
	public Vector2 speedMoveHard;
	public Vector2 speedMoveHard1;
	public Vector2 speedMoveHard2;
	private float speedMove = 0;
	private MoveDir dir = MoveDir.Left;

	private Vector3 startPos;
	private Vector3 leftPos;
	private Vector3 rightPos;

	void Start(){
		
	}

	public void MoveToStart(bool di, MoveDir m, GameController.LevelType t){
		dir = m;
		startPos = new Vector3 (posX, transform.position.y, transform.position.z);
		leftPos = new Vector3 (deltaX.x, transform.position.y, transform.position.z);
		rightPos = new Vector3 (deltaX.y, transform.position.y, transform.position.z);
		transform.position = new Vector3 (di ? 10 : -10, transform.position.y, transform.position.z);

		switch(t){
		case GameController.LevelType.Hard:
			speedMove = UnityEngine.Random.Range (speedMoveHard.x,speedMoveHard.y);
			break;

		case GameController.LevelType.Hard1:
			speedMove = UnityEngine.Random.Range (speedMoveHard1.x,speedMoveHard1.y);
			break;

		case GameController.LevelType.Hard2:
			speedMove = UnityEngine.Random.Range (speedMoveHard2.x,speedMoveHard2.y);
			break;
		}

		StartCoroutine (ToStart());
	}

	IEnumerator ToStart(){
		while(transform.position != startPos){
			yield return new WaitForSeconds (Time.deltaTime);
			transform.position = Vector3.MoveTowards (transform.position, startPos, speedInit * Time.deltaTime);
		}
		if (dir != MoveDir.None) {
			StartCoroutine (Move());
		}
	}

	IEnumerator Move(){
		while(true){
			yield return new WaitForSeconds (Time.deltaTime);
			if (dir == MoveDir.Left) {
				transform.position = Vector3.MoveTowards (transform.position, leftPos, speedMove * Time.deltaTime);
				if (transform.position.x <= deltaX.x) {
					dir = MoveDir.Right;
				}
			} else if (dir == MoveDir.Right) {
				transform.position = Vector3.MoveTowards (transform.position, rightPos, speedMove * Time.deltaTime);
				if (transform.position.x >= deltaX.y) {
					dir = MoveDir.Left;
				}
			}
			if(dir == MoveDir.None){				
				break;
			}
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "delete") {
			Destroy (gameObject);
		}
	}

	public void CollisionEvent(){		
		effect.Play ();
		dir = MoveDir.None;
		posX = transform.position.x;
		GameObject.Find ("Main Camera").GetComponent<CameraController> ().NextPoint (transform.position.z,posX);
	}

	public enum MoveDir{
		None,
		Left,
		Right
	}
}

