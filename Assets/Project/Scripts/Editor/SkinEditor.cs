﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(SkinsController))]
public class SkinEditor : Editor
{
	private SkinsController sc;
    private ReorderableList skins;
    private bool showSkins = true;

    private void OnEnable()
    {
		sc = (SkinsController)target;

        skins = new ReorderableList(serializedObject,
                serializedObject.FindProperty("skins"),
                true, true, true, true);
    }

    public override void OnInspectorGUI()
    {
		ClearAllButton ();
        BuyingCleanButton();
        ClearNoAdsButton();

		EditorGUILayout.BeginHorizontal("Box");
		GUILayout.Label("Levels on database : " + sc.skins.Count);
		EditorGUILayout.EndHorizontal();

		serializedObject.Update();

		skins.drawElementCallback =
			(Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = skins.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;

			element.FindPropertyRelative("number").intValue = index;

			element.FindPropertyRelative("open").boolValue = EditorGUI.Foldout(
				new Rect(rect.x + 10 , rect.y, 30, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("open").boolValue, "", new GUIStyle(EditorStyles.foldout));
			
			EditorGUI.LabelField(new Rect(rect.x + 10, rect.y, 285, EditorGUIUtility.singleLineHeight),
				new GUIContent("Index : " + index));
			EditorGUI.PropertyField(
				new Rect(rect.x + 70, rect.y, 185, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("name"), GUIContent.none);

			if (element.FindPropertyRelative("open").boolValue == true)
			{
				EditorGUI.LabelField(new Rect(rect.x + 10,  rect.y + EditorGUIUtility.singleLineHeight, 285, EditorGUIUtility.singleLineHeight),
					new GUIContent("Cost : "));
				EditorGUI.PropertyField(
					new Rect(rect.x + 70, rect.y + EditorGUIUtility.singleLineHeight, 185, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("cost"), GUIContent.none);


				EditorGUI.LabelField(new Rect(rect.x + 10,  rect.y + EditorGUIUtility.singleLineHeight * 2, 285, EditorGUIUtility.singleLineHeight),
					new GUIContent("Skin : "));
				element.FindPropertyRelative("skin").objectReferenceValue = EditorGUI.ObjectField(
					new Rect(rect.x + 70, rect.y + EditorGUIUtility.singleLineHeight * 2, EditorGUIUtility.singleLineHeight * 11, EditorGUIUtility.singleLineHeight * 2),
					GUIContent.none,
					element.FindPropertyRelative("skin").objectReferenceValue,
					typeof(GameObject));

				CostType t = (CostType)element.FindPropertyRelative("costType").enumValueIndex;

				t = (CostType)EditorGUI.EnumPopup(new Rect(rect.x + 10,  rect.y + EditorGUIUtility.singleLineHeight * 4, 285, EditorGUIUtility.singleLineHeight),
					"Cost type", t);
				element.FindPropertyRelative("costType").enumValueIndex = (int)t;
			}
		};

		skins.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Skins List");
		};

		skins.elementHeightCallback = (index) =>
		{
			Repaint();
			float height = 0;
			var element = skins.serializedProperty.GetArrayElementAtIndex(index);
			if (element.FindPropertyRelative("open").boolValue == true)
			{
				height = EditorGUIUtility.singleLineHeight * 6;
			}
			else
			{
				height = EditorGUIUtility.singleLineHeight * 1;
			}

			return height;
		};

		ShowMenu(true, skins);

		serializedObject.ApplyModifiedProperties();

    }

	private void ClearAllButton()
	{
		if (GUILayout.Button("Clear All"))
		{
			PlayerPrefs.DeleteAll ();
		}
	}

	private void BuyingCleanButton()
	{
		if (GUILayout.Button("Clear buying skins"))
		{
			PlayerPrefs.SetString("skins", "0!0");
			Debug.Log("Buying skins clear!");
		}
	}

    private void ClearNoAdsButton()
    {
        if (GUILayout.Button("Clear Noads"))
        {
			Settings.NOADS = false;
            Debug.Log("noads clear!");
        }
    }

    private void ShowMenu(bool b, ReorderableList r)
    {
        if (b == true)
        {
            r.elementHeight = EditorGUIUtility.singleLineHeight;
            r.displayRemove = true;
            r.displayAdd = true;
            r.DoLayoutList();
        }
        else
        {
            r.elementHeight = 0;
            r.displayRemove = false;
            r.displayAdd = false;
        }
    }
}
