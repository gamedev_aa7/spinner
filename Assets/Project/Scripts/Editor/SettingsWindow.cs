﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Linq;
public class SettingsWindow
{
    //[MenuItem("MySettings/Edit/settings")]
    //public static void OpenAnalyticItem()
    //{
    //    OpenObjectOFType<MyAnalytic>();
    //}


    [MenuItem("MySettings/Open/Analytic")]
    public static void OpenAnalyticItem()
    {
        OpenObjectOFType<MyAnalytic>();
    }

    [MenuItem("MySettings/Open/SocialNetworks")]
    public static void OpenSocialNetworks()
    {
        OpenObjectOFType<SocialNetworks>();
    }

    [MenuItem("MySettings/Open/GameController")]
    public static void OpenGameController()
    {
        OpenObjectOFType<GameController>();
    }

    [MenuItem("MySettings/Open/Settings")]
    public static void OpenSettings()
    {
        OpenObjectOFType<Settings>();
    }

    [MenuItem("MySettings/Open/SkinsController")]
    public static void OpenSkinsControllers()
    {
        OpenObjectOFType<SkinsController>();
    }

    [MenuItem("MySettings/Open/SwipeManager")]
    public static void OpenSwipeManager()
    {
        OpenObjectOFType<SwipeManager>();
    }

    [MenuItem("MySettings/Open/Scenes/UIsettings")]
    public static void OpenScene()
    {
        OpenObjectOFType<SceneController>();
    }

    [MenuItem("MySettings/Open/Scenes/Main")]
    public static void OpenSceneMain()
    {
        OpenObjectOFType<MainScreenController>();
    }

    [MenuItem("MySettings/Open/Scenes/Game")]
    public static void OpenSceneGame()
    {
        OpenObjectOFType<GameScreenController>();
    }

    [MenuItem("MySettings/Open/Scenes/Shop")]
    public static void OpenSceneShop()
    {
        OpenObjectOFType<ShopScreenController>();
    }

    [MenuItem("MySettings/Open/Scenes/ShopSkin")]
    public static void OpenSceneShopSkin()
    {
        OpenObjectOFType<SkinsShopScreen>();
    }

    public static void OpenObjectOFType<T> () where T : MonoBehaviour
    {
        var winds = Resources.FindObjectsOfTypeAll<EditorWindow>();

        var inspector = winds.Where(t => t.title == "UnityEditor.InspectorWindow").ToList();
        if (inspector.Count == 0)
        {
            //EditorUtility.DisplayDialog("Hint", "Откройте окно Inspector для изменения настроек", "ok");
        }
        GameObject o = null;
        //if (GameObject.FindObjectOfType<T>())
        T[] objs = Resources.FindObjectsOfTypeAll<T>();
        if (objs.Length>0)
        {
            o = (GameObject)objs[0].gameObject;
        }

        if (o == null)
        {
            EditorUtility.DisplayDialog("Информация", "Объект " + typeof(T).ToString() +" не найден или не активен", "ok");
        }
        else
        {
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = o;
        }
    }

}
