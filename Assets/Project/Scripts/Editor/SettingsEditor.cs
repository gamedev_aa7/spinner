﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Settings))]
public class SettingsEditor : Editor
{
    private Settings a;
    private bool showSkins = true;
    private void OnEnable()
    {
        a = (Settings)target;
    }

    public override void OnInspectorGUI()
    {
        ClearAllButton();
        EditorGUILayout.BeginHorizontal("Box");

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal("Box");
        GUILayout.Label("Settings");
        EditorGUILayout.EndHorizontal();

        //default
        DrawDefaultInspector();

        serializedObject.Update();

        serializedObject.ApplyModifiedProperties();
    }

    private void ClearAllButton()
    {
        if (GUILayout.Button("Clear All"))
        {
            PlayerPrefs.DeleteAll();
            Debug.LogError("ALL PLAYER PREFS CLEAR!!!");
        }
    }
}

