﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MyAnalytic))]
public class AnalyticEditor : Editor
{
    private MyAnalytic a;
    private bool showSkins = true;

    private void OnEnable()
    {
        a = (MyAnalytic)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginHorizontal("Box");
        string name = "not found";
        if (a.analytic != null)
        {
            name = a.analytic.analyticName;
            if (name == "")
            {
                name = a.analytic.analyticName = a.analytic.GetType().ToString();
            }
        }
        GUILayout.Label("Analytic name : " + name);
        EditorGUILayout.EndHorizontal();

        //default
        DrawDefaultInspector();

        if (a.analytic != null)
        {
            Analytic acomponent = a.analytic.GetComponent(typeof(Analytic)) as Analytic;
            if (acomponent == null)
            {
                Debug.LogWarning("Object not contain component of type \"Analytic\"");
                a.analytic = null;
            }
        }

        serializedObject.Update();

        serializedObject.ApplyModifiedProperties();
    }

    private void ClearAllButton()
    {
        if (GUILayout.Button("Clear All"))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}