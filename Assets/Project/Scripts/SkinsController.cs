﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsController : MonoBehaviour {

	public delegate void emptyDel();

	public List<Skin> skins = new List<Skin>();
	private emptyDel UpdateSkins;

	void Start(){
		if (PlayerPrefs.GetString ("skins").Split('!').Length < 2) {
			PlayerPrefs.SetString ("skins", "0!0");
		}
	}

	public void SetUpdateSkinsCallback(emptyDel ed){
		UpdateSkins = ed;
	}

	public void BuySkin(int i){
		if (i >= 0 && i < skins.Count)
		{
			string[] val = PlayerPrefs.GetString("skins").Split('!');
			string[] buying = val[1].Split(',');
			bool buy = true;
			for (int j = 0; j < buying.Length; j++)
			{
				if (i == Convert.ToInt32(buying[j]))
				{
					buy = false;
					break;
				}
			}

			if (buy == true)
			{
				PlayerPrefs.SetString("skins", val[0] + "!" + val[1]+","+i);
			}
		}
	}

	public int GetActiveIndex()
	{
		string s = PlayerPrefs.GetString("skins");
		int num = 0;
		try
		{
			num = Convert.ToInt32(s.Split('!')[0]);
			if(num < 0 || num >= skins.Count ){
				num = -1;
			}
		}
		catch(FormatException e){}
		return num;
	}

	public Skin GetActive()
	{
		int i = GetActiveIndex();
		if (i < skins.Count) {
			return skins[i];
		}
		return null;
	}

	public void SetActive(int i)
	{
		if(i >= 0 && i < skins.Count){
			string[] val = PlayerPrefs.GetString("skins").Split('!');
			string[] buying = val[1].Split(',');
			bool buy = false;
			for (int j = 0; j < buying.Length; j++ )
			{
				if(i == Convert.ToInt32(buying[j])){
					buy = true;
					break;
				}
			}

			if(buy == true){
				PlayerPrefs.SetString("skins",i+"!"+val[1]);
				//UpdateSkins();
				///player.GetComponent<PlayerController>().UpdateSkin();   NEED SET INITIALIZE SKIN!!!
			}
		}
	}

	public bool CheckPurchased(int num)
	{
		//Debug.Log(PlayerPrefs.GetString("skins"));
        string[] va = (PlayerPrefs.GetString("skins").Split('!'));
		//Debug.Log(va.Length);
		//Debug.Log(PlayerPrefs.GetString("skins"));
        string[] val = va[1].Split(',');
        int[] b = new int[val.Length];
        for (int i = 0; i < b.Length; i++)
        {
            b[i] = Convert.ToInt32(val[i]);
        }

        for (int i = 0; i < b.Length; i++)
        {
            if (b[i] == num)
            {
                return true;
            }
        }
        return false;
	}
}

[Serializable]
public class Skin{
	public GameObject skin;
	public string name;
	public int cost = 0;
	public int number;
	public bool open = false;
	public CostType costType = CostType.Coins;
}

public enum CostType{
	Coins,
	Facebook,
	Twitter
}
