﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using com.playGenesis.VkUnityPlugin;
using com.playGenesis.VkUnityPlugin.MiniJSON;

public class FriendsController : MonoBehaviour {

    public VkApi vkApi;
    public Downloader downloader;
    public List<VKUser> friends = new List<VKUser>();
    public VkSettings settings;
    public GameObject friendPrefab, scrollContent;
    public static float nFriend = 1;

	public int friendsCount = 30;
	private int offset = 0;

    void Start()
    {
        vkApi = VkApi.VkApiInstance;
        settings = VkApi.VkSetts;        
        downloader = vkApi.gameObject.GetComponent<Downloader>();
        if (vkApi.IsUserLoggedIn) {
            //StartWorking();            
        }
    }

    void StartWorking() {
		if (VKToken.TokenValidFor () < 120) {
			vkApi.Login ();
		}
		GetFriendsData();
    }

    public void GetFriendsData() {
        var request = new VKRequest() {
			url = "friends.get?user_id=" + VkApi.CurrentToken.user_id + "&count="+friendsCount+"&offset="+offset*friendsCount+"&fields=photo_50&v=" + VkApi.VkSetts.apiVersion,
            CallBackFunction = OnGetFriendsCompleted,
        };
        vkApi.Call(request);
    }

    void OnGetFriendsCompleted(VKRequest arg) {
        if (arg.error != null)
        {
			if (offset > 0) {
				offset--;
			}
			print("error load friends " + arg.error.error_code);
            FindObjectOfType<GlobalErrorHandler>().Notification.Notify(arg);
            return;
        }

        var dict = Json.Deserialize(arg.response) as Dictionary<string, object>;
		if (dict == null) {
			if (offset > 0) {
				offset--;
			}
			print ("ERROR | OnGetFriendsCompleted | dict == null");
			return;
		}

        var resp = (Dictionary<string, object>)dict["response"];
		if (resp == null) {
			if (offset > 0) {
				offset--;
			}
			print ("ERROR | OnGetFriendsCompleted | resp == null");
			return;
		}

        var items = (List<object>)resp["items"];
		if (items == null) {
			if (offset > 0) {
				offset--;
			}
			print ("ERROR | OnGetFriendsCompleted | items == null");
			return;
		}

        foreach (var item in items)
        {
            friends.Add(VKUser.Deserialize(item));
        }
        nFriend = friends.Count;
        for (int i = 0; i < friends.Count; i++) {
            GameObject temp = Instantiate(friendPrefab);
            temp.transform.parent = scrollContent.transform;
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        var friendsOnScene = GameObject.FindObjectsOfType<FriendManager>();
        
		for (var i = 0; i < friends.Count; i++)
        {
            Action<DownloadRequest> doOnFinish = (downloadRequest) =>
            {
                var friendCard = (FriendManager)downloadRequest.CustomData[0];
				if(downloadRequest.DownloadResult.texture.width >= 50){
                	friendCard.setUpImage(downloadRequest.DownloadResult.texture);
				}
				else{
					print("Load pict error");
				}

            };
            friendsOnScene[i].t.text = friends[i].first_name + " " + friends[i].last_name;
            friendsOnScene[i].friend = friends[i];
            var request = new DownloadRequest
            {
                url = friends[i].photo_50,
                onFinished = doOnFinish,
                CustomData = new object[] { friendsOnScene[i] }
            };
            downloader.download(request);
        }
    }

	private bool CanLoad = true;

	public void ButtonNext(){
		if(CanLoad == true){
			StartCoroutine (Delay (Next));
		}
	}

	void Next(){
		if (friends.Count == friendsCount) {
			ClearFriendsList ();
			offset++;
			GetFriendsData ();
		}
	}

	public void ButtonPrevious(){
		if(CanLoad == true){
			StartCoroutine (Delay (Previous));
		}
	}

	void Previous(){		
		ClearFriendsList ();
		if (offset > 0) {
			offset--;
		}
		GetFriendsData ();	
	}

	public void ClearList(){
		ClearFriendsList ();
		offset = 0;
		GetFriendsData ();
	}

	public void ClearFriendsList()
	{
		friends.Clear ();
		foreach(Transform child in scrollContent.transform) {
			Destroy(child.gameObject);
		}
	}

	IEnumerator Delay(mydel d){
		CanLoad = false;
		yield return new WaitForSeconds (0.3f);
		d ();
		CanLoad = true;
	}
}

delegate void mydel();

